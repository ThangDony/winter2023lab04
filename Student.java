public class Student {

	private String name;
	private String courseName;
	private int finalGrade;
	public int amountLearnt;

	public Student(String name, String courseName) {

		this.name = name;
		this.courseName = courseName;
		this.finalGrade = 93;
		this.amountLearnt = 0;
	}

public void learn(int amountStudied) {
	System.out.println("Before: " + amountStudied);
	if (amountStudied > 0) {
		amountLearnt += amountStudied;
	}
	System.out.println("After: " + amountLearnt);
}
	
	public void study() {
		System.out.println("Gotta study 24/7 cuz that's what school wants us to do");
		
	}
	
	public void passOrFail() {
		if (finalGrade < 60) {
			System.out.println("You are a disappointment, get good.");
		} else if (finalGrade > 60 && finalGrade < 80) {
			System.out.println("You did fine, could have been better though.");
		} else {
			System.out.println("Great job, proud of you.");
		}
	}

	public int getAmountLearnt() {
		return this.amountLearnt;
	}

	public String getName() {
		return this.name;
	}

	public String getCourseName() {

		return this.courseName;
	}

	public int getFinalGrade() {
		return this.finalGrade;
	}

	public void setFinalGrade(int newFinalGrade) {
		this.finalGrade = newFinalGrade;
	}
}
