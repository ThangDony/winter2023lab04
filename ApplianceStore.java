import java.util.Scanner;    
public class ApplianceStore {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        Microwave[] microwaveSet = new Microwave[4];

        String brand;
        int volt = 0;
        double price = 0;
        int timeInSeconds = 0;
        
        for (int i = 0; i < 2; i++) {
            /* Not putting the whole microwaveSet.length because I don't want to repeat 
             this code over and over */

            System.out.println("----------------------------");
            
            System.out.println("Brand: ");
            brand = keyboard.next();
            
            System.out.println("Voltage: ");
            volt = keyboard.nextInt();
            
            System.out.println("Price: ");
            price = keyboard.nextDouble();

            System.out.println("Cooking time (seconds): ");
            timeInSeconds = keyboard.nextInt();

            if (i ==0) {
                microwaveSet[i] = new Microwave(brand, volt, price, timeInSeconds);
            } else {
                microwaveSet[i] = new Microwave(volt, price, timeInSeconds);
            }
            
        }
        System.out.println("Before setters call: ");
        System.out.println("Brand: " + microwaveSet[1].getBrand() + " | Volt: " + microwaveSet[1].getVolt());
        System.out.println("Price: " + microwaveSet[1].getPrice() + " | Time: " + microwaveSet[1].getTimeInSeconds());
        microwaveSet[1].setVolt(50);
        microwaveSet[1].setPrice(51);
        microwaveSet[1].setTimeInSeconds(52);
        System.out.println("After setters call: ");
        System.out.println("Brand: " + microwaveSet[1].getBrand() + " | Volt: " + microwaveSet[1].getVolt());
        System.out.println("Price: " + microwaveSet[1].getPrice() + " | Time: " + microwaveSet[1].getTimeInSeconds());

       keyboard.close();

    }
}
