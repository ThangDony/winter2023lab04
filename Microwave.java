import java.time.LocalTime;
import java.util.Scanner;

public class Microwave {

    private String brand;
    private int volt;
    private double price;
    private int timeInSeconds;

    public Microwave(String brand, int volt, double price, int timeInSeconds) {
        this.brand = brand;
        this.volt = volt;
        this.price = price;
        this.timeInSeconds = timeInSeconds;
    }

    public Microwave(int volt, double price, int timeInSeconds) {
        this.volt = volt;
        this.price = price;
        this.timeInSeconds = timeInSeconds;
    }

    // GETTER METHODS
    public String getBrand() {
        return this.brand;
    }

    public int getVolt() {
        return this.volt;
    }

    public double getPrice() {
        return this.price;
    }

    public int getTimeInSeconds() {
        return this.timeInSeconds;
    }

    public void setVolt(int volt) {
        this.volt = volt;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public void setTimeInSeconds(int timeInSeconds) {
        this.timeInSeconds = timeInSeconds;
    }

    // INSTANCE METHODS

    public void heatUp() {
        if (volt >= 120) {
            System.out.println("Heating up whatever you put in there! :)");
        } else {
            System.out.println("Uh oh, seems like there is not enough power for the microwave to properly work :(");
        }
    }

    public void printClock() {
        LocalTime time = LocalTime.now();
        System.out.println("Clock: " + time);
    }
    
    public void cook(int timeInSeconds) {
        timeInSeconds = validateInput(timeInSeconds);
        this.timeInSeconds = timeInSeconds;
        System.out.println("Microwave cooking for " + timeInSeconds + " seconds.");
    }

    private int validateInput(int timeInSecondsInput) {
        Scanner keyboard = new Scanner(System.in);
        if (timeInSecondsInput > 0) {
            return timeInSecondsInput;
        }
        do {
            System.out.println("Invalid cooking time: " + timeInSecondsInput + "seconds. Please enter a positive value.");
            timeInSecondsInput = keyboard.nextInt(); 
            } 
            while (timeInSecondsInput <= 0);
            return timeInSecondsInput;   
    }
}

